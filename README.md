StackoverLike is a Grails-based StackOverflow-like project.

# Default users

## Development and test environments
* admin / admin - with Administrator rights
* user / user - with standard rights

## Production environment
* Administrator / Dra*2017 - with Administrator rights
* Gandalf / AWizardIsNeverLate! - with standard rights
