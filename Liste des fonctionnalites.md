Les fonctionnalités suivantes ont été misent en place sur la version 2 de l'application.

* Feature flipping
	* L'état de chaque feature est stocké en base de données.
	* Possibilité de changer l'état d'une feature par requête http aux urls suivantes:
		* /feature?name=<feature-name>&enabled=true
		* /feature?name=<feature-name>&enabled=false
	* Les features disponibles à l'activation/désactivation sont: vote, badge, auth.
	* Le feature flipping s'applique correctement dans le backend de l'application mais n'est pas pris en compte dans l'UI.
* Application stateless
	* API REST complète permettant d'interagir avec l'ensemble de l'application.
	* Client angular single page.
		* L'intégration d'AngularJS nous ayant posé beaucoup de difficultés au début (à cause du routing notament),
			nous n'avons pas pu intégrer toutes les fonctionnalités telles que les votes (ajouts, édition, ...)
* Authentification stateless, par token (avec spring-security-rest).
	* Le token utilisé est porteur du username et des roles, ce qui permet au client d'utiliser ces informations au besoin.
* Health check
	* Url: /health
* Graceful degradation
	* Utilisation d'hystrix (composant Spring cloud)
	* Intégration du service externe OpenWeatherMap
		* Récupère la température courante à Clermont-Ferrand en temps normal.
		* Donne la température moyenne à Cuba en cas d'indisponibilité du service.

