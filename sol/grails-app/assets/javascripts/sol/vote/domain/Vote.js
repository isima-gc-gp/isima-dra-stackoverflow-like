//= wrapped
//= require /angular/angular-resource

angular
    .module("sol.vote")
    .factory("Vote", Vote);

function Vote($resource) {
    var Vote = $resource(
        "vote/:id",
        {"id": "@id"},
        {"update": {method: "PUT"},
         "query": {method: "GET", isArray: true},
         "get": {method: 'GET'}}
    );

    Vote.list = Vote.query;

    Vote.prototype.toString = function() {
        return 'sol.Vote : ' + (this.id ? this.id : '(unsaved)');
    };

    return Vote;
}
