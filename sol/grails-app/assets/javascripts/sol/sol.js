//= wrapped
//= require /angular/angular
//= require /angular/angular-resource
//= require /angular/angular-ui-router
//= require /sol/core/sol.core
//= require /sol/index/sol.index
//= require /sol/appUser/sol.appUser
//= require /sol/appUserBadge/sol.appUserBadge
//= require /sol/vote/sol.vote
//= require /sol/post/sol.post
//= require /sol/tag/sol.tag
//= require /sol/question/sol.question
//= require /sol/response/sol.response
//= require_self
//= require_tree services
//= require_tree controllers
//= require_tree directives
//= require_tree domain
//= require_tree templates

angular.module("sol", [
    "sol.core",
    "sol.index",
    "ui.router",
    "sol.appUser",
    "sol.appUserBadge",
    "sol.vote",
    "sol.post",
    "sol.tag",
    "sol.question",
    "sol.response"
]);

	