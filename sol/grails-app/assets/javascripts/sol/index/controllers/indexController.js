//= wrapped

angular
    .module("sol.index")
    .config(function ($httpProvider) {
        $httpProvider.interceptors.push(function ($rootScope, $window) {
        return {
            request: function (config) {
                config.headers = config.headers || {};
                if ($window.sessionStorage.token) {
                    config.headers.Authorization = 'Bearer ' + $window.sessionStorage.token;
                }
                return config;
            }
        };});
    })
    .controller("IndexController", IndexController);

function IndexController(applicationDataFactory, contextPath, $http, $state, $window) {
    var vm = this;
	
	function urlBase64Decode(str) {
		var output = str.replace('-', '+').replace('_', '/');
		switch (output.length % 4) {
			case 0:
				break;
			case 2:
				output += '==';
				break;
			case 3:
				output += '=';
				break;
			default:
				throw 'Illegal base64url string!';
		}
		return window.atob(output);
	}
	
	function getUserFromToken() {
		var token = $window.sessionStorage.token;
		var user = null;
		if (typeof token !== 'undefined') {
			var encoded = token.split('.')[1];
			user = JSON.parse(urlBase64Decode(encoded));
		}
		return user;
	}
 
    vm.currentUser = getUserFromToken();
	
	vm.currentUserHasRole = function(role) {
		if (vm.currentUser == null) return false;
		var i = vm.currentUser.roles.length;
		while (i--) {
			if (vm.currentUser.roles[i] == role) {
				return true;
			}
		}
		return false;
	}

    vm.contextPath = contextPath;

    applicationDataFactory.get().then(function(response) {
        vm.applicationData = response.data;
    });

    vm.stateExists = function(name) {
        return $state.get(name) != null;
    };

    // Retrieve weather data
	$http.get('/weather', {}).then(function (response) {
        var widgetDiv = document.getElementById('navbar-weather');
        widgetDiv.innerHTML = response.data;
    });
}
