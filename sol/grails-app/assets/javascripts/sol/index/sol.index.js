//= wrapped
//= require /angular/angular
//= require /angular/ui-bootstrap-tpls
//= require /angular/angular-ui-router
//= require_self
//= require_tree services
//= require_tree controllers
//= require_tree directives
//= require_tree templates

angular.module("sol.index", [
    "sol.core",
    "ui.bootstrap.dropdown",
    "ui.bootstrap.collapse",
    "ui.router"
])
.config(config);

function config($stateProvider, $urlRouterProvider) {
    $stateProvider
        .state('index', {
            url: "/",
            templateUrl: "/sol/question/list.html",
			controller: "QuestionListController as vm"            
        })
        .state('login', {
            url: "/login",
            templateUrl: "/sol/index/login.html",
			controller: "LoginController as loginCtrl"
        })
        .state('logout', {
            url: "/logout",
			controller: function($scope, $state, $window) {
				$scope.indexCtrl.currentUser = null;
				delete $window.sessionStorage.token;
				$state.go('index');
			}
        });

    $urlRouterProvider.otherwise('/');
}
