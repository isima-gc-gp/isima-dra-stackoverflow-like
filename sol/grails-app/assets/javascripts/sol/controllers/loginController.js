//= wrapped

angular
    .module("sol")
    .controller("LoginController", ['$http', '$window', '$scope', '$location', LoginController]);

function LoginController($http, $window, $scope, $location) {
    var vm = this;
	
	function urlBase64Decode(str) {
		var output = str.replace('-', '+').replace('_', '/');
		switch (output.length % 4) {
			case 0:
				break;
			case 2:
				output += '==';
				break;
			case 3:
				output += '=';
				break;
			default:
				throw 'Illegal base64url string!';
		}
		return window.atob(output);
	}
	
	function getUserFromToken() {
		var token = $window.sessionStorage.token;
		var user = null;
		if (typeof token !== 'undefined') {
			var encoded = token.split('.')[1];
			user = JSON.parse(urlBase64Decode(encoded));
		}
		return user;
	}

    vm.user = {};
	
	vm.login = function () {
        $http.post('/api/login', {
            username: vm.user.username,
            password: vm.user.password
        }).then(function (response) {
            $window.sessionStorage.token = response.data.access_token;
			$scope.indexCtrl.currentUser = getUserFromToken();
			$location.url("/");
        }, function (response)
        {
        	document.getElementById("error").innerHTML = "Please, check your ids."
        	document.getElementById("error").className += " errors";
        });
    };
}

