//= wrapped
//= require /angular/angular-resource

angular
    .module("sol.question")
    .factory("Question", Question);

function Question($resource) {
    var Question = $resource(
        "question/:id",
        {"id": "@id"},
        {"update": {method: "PUT"},
         "query": {method: "GET", isArray: true},
         "get": {method: 'GET'}}
    );

    Question.list = Question.query;

    Question.prototype.toString = function() {
        return 'sol.Question : ' + (this.id ? this.id : '(unsaved)');
    };

    return Question;
}
