//= wrapped
//= require /angular/angular 
//= require /angular/angular-ui-router
//= require /angular/angular-resource
//= require /sol/core/sol.core
//= require /sol/tag/sol.tag
//= require /sol/vote/sol.vote
//= require /sol/appUser/sol.appUser
//= require /sol/response/sol.response
//= require_self
//= require_tree services
//= require_tree controllers
//= require_tree directives
//= require_tree domain
//= require_tree templates

angular.module("sol.question", [
    "ui.router",
    "ngResource",
    "sol.core",
    "sol.tag",
    "sol.vote",
    "sol.appUser",
    "sol.response"
]).config(config);

function config($stateProvider) {
    $stateProvider
        .state('question', {
            url: "/question",
            abstract: true,
            template: "<div ui-view></div>"
        })
        .state('question.list', {
            url: "",
            templateUrl: "/sol/question/list.html",
            controller: "QuestionListController as vm"
        })
        .state('question.create', {
            url: "/create",
            params: {"userId":null},
            controller: function($scope, $state) {
				if ($scope.indexCtrl.currentUserHasRole('ROLE_USER'))
					$state.go('question.create-allowed');
				else
					$state.go('login');
			  }
        })
        .state('question.create-allowed', {
            url: "/create/",
            templateUrl: "/sol/question/create.html",
            controller: "QuestionCreateController as vm"
        })
        .state('question.edit', {
            url: "/edit/:id",
            templateUrl: "/sol/question/edit.html",
            controller: function($scope, $state) {
				if ($scope.indexCtrl.currentUserHasRole('ROLE_USER'))
					$state.go('question.edit-allowed', {'id': $state.params.id});
				else
					$state.go('login');
			  }
        })
        .state('question.edit-allowed', {
            url: "/edit/:id",
            templateUrl: "/sol/question/edit.html",
            controller: "QuestionEditController as vm"
        })
        .state('question.show', {
            url: "/show/:id",
            templateUrl: "/sol/question/show.html",
            controller: "QuestionShowController as vm"
        });
}
