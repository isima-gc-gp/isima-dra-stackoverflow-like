//= wrapped

angular
    .module("sol.question")
    .controller("QuestionShowController", QuestionShowController)

function QuestionShowController(Question, $scope, $stateParams, $state, Response) {
    var vm = this;

    Question.get({id: $stateParams.id}, function(data) {
        vm.question = new Question(data);
    }, function() {
        $state.go('question.list');
    });

    vm.delete = function() {
        if(confirm("Are you sure you want to delete this question ?")) {
            vm.question.$delete(function() {
                $state.go('question.list');
            }, function() {
                // on error
            });
        }
    };

    vm.deleteResponse = function(res) {
        if(confirm("Are you sure you want to delete this answer ?")) {
            Response.delete({action:'delete'}, res, function() {
                $state.reload();
            }, function()
            {
                // on error
            });
        }
    };

    vm.computeVoteScore = function(post) {
        var total = 0;
        if (post !== undefined)
        {
            for (var i = 0; i < post.votes.length; i++)
            {
                total += post.votes[i].isUpVote ? 1 : -1;
            }
        }

        return total;
    };

    vm.compareVoteScore = function(p1, p2)
    {
		if (p1.type !== 'object' || p2.type !== 'object') {
			return (p1.index < p2.index) ? -1 : 1;
		}
        var vs1 = vm.computeVoteScore(p1.value);
        var vs2 = vm.computeVoteScore(p2.value);
        if (vs1 == vs2)
            return p1.value.lastUpdated < p2.value.lastUpdated;
        else
            return vs1 < vs2;
    };
}