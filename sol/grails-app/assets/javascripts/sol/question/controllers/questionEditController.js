//= wrapped

angular
    .module("sol.question")
    .controller("QuestionEditController", QuestionEditController);

function QuestionEditController(Question, $stateParams, $state, Tag, Vote, AppUser, Response) {
    var vm = this;

    vm.tagList = Tag.list();

    Question.get({id: $stateParams.id}, function(data) {
        vm.question = new Question(data);
    }, function() {
        vm.errors = [{message: "Could not retrieve question with ID " + $stateParams.id}];
    });

    vm.updateQuestion = function() {
        vm.errors = undefined;
		var questionId = vm.question.id;
        vm.question.$update(function() {
            $state.go('question.show', {id: questionId});
        }, function(response) {
            var data = response.data;
            if (data.hasOwnProperty('message')) {
                vm.errors = [data];
            } else {
                vm.errors = data._embedded.errors;
            }
        });
    };
}
