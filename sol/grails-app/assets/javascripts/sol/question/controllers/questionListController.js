//= wrapped

angular
    .module("sol.question")
    .controller("QuestionListController", QuestionListController);

function QuestionListController(Question) {
    var vm = this;

    var max = 10, offset = 0;

    Question.list({max: max, offset: offset}, function(data) {
        vm.questions = data;
    });
}
