//= wrapped

angular
    .module("sol.question")
    .controller("QuestionCreateController", QuestionCreateController);

function QuestionCreateController(Question, $state, $stateParams, Tag, Vote, AppUser, Response) {

    var vm = this;
    vm.tagList = Tag.list();
    vm.question = new Question();
    
    if ($stateParams.userId) {
        vm.question.user = {id: $stateParams.userId};
    }
    
    vm.saveQuestion = function() {
        vm.errors = undefined;
        vm.question.$save({}, function() {
            $state.go('question.show', {id: vm.question.id});
        }, function(response) {
            var data = response.data;
            if (data.hasOwnProperty('message')) {
                vm.errors = [data];
            } else {
                vm.errors = data._embedded.errors;
            }
        });
    };
}
