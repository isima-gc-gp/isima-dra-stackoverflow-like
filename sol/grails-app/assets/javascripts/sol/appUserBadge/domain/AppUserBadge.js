//= wrapped
//= require /angular/angular-resource

angular
    .module("sol.appUserBadge")
    .factory("AppUserBadge", AppUserBadge);

function AppUserBadge($resource) {
    var AppUserBadge = $resource(
        "appUserBadge/:id",
        {"id": "@id"},
        {"update": {method: "PUT"},
         "query": {method: "GET", isArray: true},
         "get": {method: 'GET'}}
    );

    AppUserBadge.list = AppUserBadge.query;

    AppUserBadge.prototype.toString = function() {
        return 'sol.AppUserBadge : ' + (this.id ? this.id : '(unsaved)');
    };

    return AppUserBadge;
}
