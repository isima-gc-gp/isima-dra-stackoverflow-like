//= wrapped
//= require /angular/angular 
//= require /angular/angular-ui-router
//= require /angular/angular-resource
//= require /sol/core/sol.core
//= require /sol/question/sol.question
//= require_self
//= require_tree services
//= require_tree controllers
//= require_tree directives
//= require_tree domain
//= require_tree templates

angular.module("sol.tag", [
    "ui.router",
    "ngResource",
    "sol.core",
    "sol.question"
]).config(config);

function config($stateProvider) {
    $stateProvider
        .state('tag', {
            url: "/tag",
            abstract: true,
            template: "<div ui-view></div>"
        })
        .state('tag.list', {
            url: "",
            templateUrl: "/sol/tag/list.html",
            controller: "TagListController as vm"
        })
        .state('tag.create', {
            url: "/create",
            controller: function($scope, $state) {
				if ($scope.indexCtrl.currentUserHasRole('ROLE_ADMIN'))
					$state.go('tag.create-allowed');
				else
					$state.go('login');
			  }
        })
        .state('tag.create-allowed', {
            url: "/create/",
            templateUrl: "/sol/tag/create.html",
            controller: "TagCreateController as vm"
        })
        .state('tag.edit', {
            url: "/edit/:id",
            controller: function($scope, $state) {
				if ($scope.indexCtrl.currentUserHasRole('ROLE_ADMIN'))
					$state.go('tag.edit-allowed', {'id': $state.params.id});
				else
					$state.go('login');
			  }
        })
        .state('tag.edit-allowed', {
            url: "/edit/:id",
            templateUrl: "/sol/tag/edit.html",
            controller: "TagEditController as vm"
        })
        .state('tag.show', {
            url: "/show/:id",
            templateUrl: "/sol/tag/show.html",
            controller: "TagShowController as vm"
        });
}
