//= wrapped

angular
    .module("sol.tag")
    .controller("TagShowController", TagShowController);

function TagShowController(Tag, $stateParams, $state) {
    var vm = this;

    Tag.get({id: $stateParams.id}, function(data) {
        vm.tag = new Tag(data);
    }, function() {
        $state.go('tag.list');
    });

    vm.delete = function() {
		if (!$scope.indexCtrl.currentUserHasRole('ROLE_ADMIN')) return;
        vm.tag.$delete(function() {
            $state.go('tag.list');
        }, function() {
            //on error
        });
    };

}
