//= wrapped
//= require /angular/angular-resource

angular
    .module("sol.tag")
    .factory("Tag", Tag);

function Tag($resource, domainListConversion, domainToManyConversion) {
    var Tag = $resource(
        "tag/:id",
        {"id": "@id"},
        {"update": {method: "PUT"},
         "query": {method: "GET", isArray: true, transformResponse: [angular.fromJson, domainListConversion("Question", "questions", "domainToManyConversion")]},
         "get": {method: 'GET', transformResponse: [angular.fromJson, domainToManyConversion("Question", "questions")]}}
    );

    Tag.list = Tag.query;

    Tag.prototype.toString = function() {
        return 'sol.Tag : ' + (this.id ? this.id : '(unsaved)');
    };

    return Tag;
}
