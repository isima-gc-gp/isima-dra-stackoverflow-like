//= wrapped
//= require /angular/angular 
//= require /sol/core/sol.core
//= require_self
//= require_tree services
//= require_tree controllers
//= require_tree directives
//= require_tree domain
//= require_tree templates

angular.module("sol.post", ["sol.core"]);