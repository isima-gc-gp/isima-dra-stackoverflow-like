//= wrapped

angular
    .module("sol.response")
    .controller("ResponseListController", ResponseListController);

function ResponseListController(Response) {
    var vm = this;

    var max = 10, offset = 0;

    Response.list({max: max, offset: offset}, function(data) {
        vm.responseList = data;
    });
}
