//= wrapped

angular
    .module("sol.response")
    .controller("ResponseCreateController", ResponseCreateController);

function ResponseCreateController(Response, $state, $stateParams, Vote, Question, AppUser) {

    var vm = this;
    vm.response = new Response();
    
    if ($stateParams.questionId) {
        vm.response.question = {id: $stateParams.questionId};
    }
    
    if ($stateParams.userId) {
        vm.response.user = {id: $stateParams.userId};
    }
    
    vm.saveResponse = function() {
        vm.errors = undefined;
		var questionId = vm.response.question.id;
        vm.response.$save({}, function() {
            $state.go('question.show', {id: questionId});
        }, function(response) {
            var data = response.data;
            if (data.hasOwnProperty('message')) {
                vm.errors = [data];
            } else {
                vm.errors = data._embedded.errors;
            }
        });
    };
}
