//= wrapped

angular
    .module("sol.response")
    .controller("ResponseEditController", ResponseEditController);

function ResponseEditController(Response, $stateParams, $state, Vote, Question, AppUser) {
    var vm = this;

    Response.get({id: $stateParams.id}, function(data) {
        vm.response = new Response(data);
    }, function() {
        vm.errors = [{message: "Could not retrieve response with ID " + $stateParams.id}];
    });

    vm.updateResponse = function() {
        vm.errors = undefined;
		var questionId = vm.response.question.id;
        vm.response.$update(function() {
            $state.go('question.show', {id : questionId});
        }, function(response) {
            var data = response.data;
            if (data.hasOwnProperty('message')) {
                vm.errors = [data];
            } else {
                vm.errors = data._embedded.errors;
            }
        });
    };
}
