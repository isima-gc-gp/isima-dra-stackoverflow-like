//= wrapped

angular
    .module("sol.response")
    .controller("ResponseShowController", ResponseShowController);

function ResponseShowController(Response, $stateParams, $state) {
    var vm = this;

    Response.get({id: $stateParams.id}, function(data) {
        vm.response = new Response(data);
    }, function() {
        $state.go('response.list');
    });

    vm.delete = function() {
        vm.response.$delete(function() {
            $state.go('response.list');
        }, function() {
            //on error
        });
    };

}
