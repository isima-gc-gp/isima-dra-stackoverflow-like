//= wrapped
//= require /angular/angular 
//= require /angular/angular-ui-router
//= require /angular/angular-resource
//= require /sol/core/sol.core
//= require /sol/vote/sol.vote
//= require /sol/question/sol.question
//= require /sol/appUser/sol.appUser
//= require_self
//= require_tree services
//= require_tree controllers
//= require_tree directives
//= require_tree domain
//= require_tree templates

angular.module("sol.response", [
    "ui.router",
    "ngResource",
    "sol.core",
    "sol.vote",
    "sol.question",
    "sol.appUser"
]).config(config);

function config($stateProvider) {
    $stateProvider
        .state('response', {
            url: "/response",
            abstract: true,
            template: "<div ui-view></div>"
        })
        .state('response.list', {
            url: "",
            templateUrl: "/sol/response/list.html",
            controller: "ResponseListController as vm"
        })
        .state('response.create', {
            url: "/create?questionId",
            params: {"questionId":null,"userId":null},
            controller: function($scope, $state) {
				if ($scope.indexCtrl.currentUserHasRole('ROLE_USER'))
					$state.go('response.create-allowed', {'questionId': $state.params.questionId});
				else
					$state.go('login');
			  }
        })
        .state('response.create-allowed', {
            url: "/create/?questionId",
            templateUrl: "/sol/response/create.html",
            controller: "ResponseCreateController as vm"
        })
        .state('response.edit', {
            url: "/edit/:id",
            controller: function($scope, $state) {
				if ($scope.indexCtrl.currentUserHasRole('ROLE_USER'))
					$state.go('response.edit-allowed', {'id': $state.params.id});
				else
					$state.go('login');
			  }
        })
        .state('response.edit-allowed', {
            url: "/edit/:id",
            templateUrl: "/sol/response/edit.html",
            controller: "ResponseEditController as vm"
        })
        .state('response.show', {
            url: "/show/:id",
            templateUrl: "/sol/response/show.html",
            controller: "ResponseShowController as vm"
        });
}
