//= wrapped
//= require /angular/angular-resource

angular
    .module("sol.response")
    .factory("Response", Response);

function Response($resource, domainListConversion, domainToManyConversion, domainConversion) {
    var Response = $resource(
        "response/:id",
        {"id": "@id"},
        {"update": {method: "PUT"},
         "query": {method: "GET", isArray: true, transformResponse: [angular.fromJson, domainListConversion("Vote", "votes", "domainToManyConversion"), domainListConversion("Question", "question", "domainConversion"), domainListConversion("AppUser", "user", "domainConversion")]},
         "get": {method: 'GET', transformResponse: [angular.fromJson, domainToManyConversion("Vote", "votes"), domainConversion("Question", "question"), domainConversion("AppUser", "user")]}}
    );

    Response.list = Response.query;

    Response.prototype.toString = function() {
        return 'sol.Response : ' + (this.id ? this.id : '(unsaved)');
    };

    return Response;
}
