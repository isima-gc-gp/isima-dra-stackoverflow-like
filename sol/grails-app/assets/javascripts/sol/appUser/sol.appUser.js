//= wrapped
//= require /angular/angular 
//= require /angular/angular-ui-router
//= require /angular/angular-resource
//= require /sol/core/sol.core
//= require /sol/appUserBadge/sol.appUserBadge
//= require /sol/vote/sol.vote
//= require /sol/post/sol.post
//= require_self
//= require_tree services
//= require_tree controllers
//= require_tree directives
//= require_tree domain
//= require_tree templates

angular.module("sol.appUser", [
    "ui.router",
    "ngResource",
    "sol.core",
    "sol.appUserBadge",
    "sol.vote",
    "sol.post"
]).config(config);

function config($stateProvider) {
    $stateProvider
        .state('appUser', {
            url: "/appUser",
            abstract: true,
            template: "<div ui-view></div>"
        })
        .state('appUser.list', {
            url: "",
            templateUrl: "/sol/appUser/list.html",
            controller: "AppUserListController as vm"
        })
        .state('appUser.create', {
            url: "/create",
            templateUrl: "/sol/appUser/create.html",
            controller: "AppUserCreateController as vm"
        })
        .state('appUser.edit', {
            url: "/edit/:id",
            controller: function($scope, $state) {
				if ($scope.indexCtrl.currentUserHasRole('ROLE_USER'))
					$state.go('appUser.edit-allowed', {'id': $state.params.id});
				else
					$state.go('login');
			  }
        })
        .state('appUser.edit-allowed', {
            url: "/edit/:id",
            templateUrl: "/sol/appUser/edit.html",
            controller: "AppUserEditController as vm"
        })
        .state('appUser.show', {
            url: "/show/:id",
            templateUrl: "/sol/appUser/show.html",
            controller: "AppUserShowController as vm"
        });
}
