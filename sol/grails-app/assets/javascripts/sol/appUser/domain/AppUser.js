//= wrapped
//= require /angular/angular-resource

angular
    .module("sol.appUser")
    .factory("AppUser", AppUser);

function AppUser($resource, domainListConversion, domainToManyConversion) {
    var AppUser = $resource(
        "appUser/:id",
        {"id": "@id"},
        {"update": {method: "PUT"},
         "query": {method: "GET", isArray: true, transformResponse: [angular.fromJson, domainListConversion("AppUserBadge", "userBadges", "domainToManyConversion"), domainListConversion("Vote", "originatedVotes", "domainToManyConversion"), domainListConversion("Post", "posts", "domainToManyConversion")]},
         "get": {method: 'GET', transformResponse: [angular.fromJson, domainToManyConversion("AppUserBadge", "userBadges"), domainToManyConversion("Vote", "originatedVotes"), domainToManyConversion("Post", "posts")]}}
    );

    AppUser.list = AppUser.query;

    AppUser.prototype.toString = function() {
        return 'sol.AppUser : ' + (this.id ? this.id : '(unsaved)');
    };

    return AppUser;
}
