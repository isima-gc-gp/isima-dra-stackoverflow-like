//= wrapped

angular
    .module("sol.appUser")
    .controller("AppUserEditController", AppUserEditController);

function AppUserEditController(AppUser, $stateParams, $state, AppUserBadge, Vote, Post) {
    var vm = this;

    vm.appUserBadgeList = AppUserBadge.list();
    vm.voteList = Vote.list();
    vm.postList = Post.list();

    AppUser.get({id: $stateParams.id}, function(data) {
        vm.appUser = new AppUser(data);
    }, function() {
        vm.errors = [{message: "Could not retrieve appUser with ID " + $stateParams.id}];
    });

    vm.updateAppUser = function() {
        vm.errors = undefined;
        console.log(vm.appUser);

        AppUser.update({action:'update'},vm.appUser,function(res){
            $state.go('appUser.show', {id: vm.appUser.id});
        }, function(res)
        {
            var data = response.data;
            if (data.hasOwnProperty('message')) {
                vm.errors = [data];
            } else {
                vm.errors = data._embedded.errors;
            }
        })
    };
}
