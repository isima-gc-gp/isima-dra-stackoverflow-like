//= wrapped

angular
    .module("sol.appUser")
    .controller("AppUserCreateController", AppUserCreateController);

function AppUserCreateController(AppUser, $state, AppUserBadge, Vote, Post) {

    var vm = this;
    vm.appUserBadgeList = AppUserBadge.list();
    vm.voteList = Vote.list();
    vm.postList = Post.list();
    vm.appUser = new AppUser();
    
    vm.saveAppUser = function() {

        console.log(vm.appUser);

        vm.errors = undefined;
        vm.appUser.$save({}, function() {
            $state.go('appUser.show', {id: vm.appUser.id});
        }, function(response) {
            var data = response.data;
            if (data.hasOwnProperty('message')) {
                vm.errors = [data];
            } else {
                vm.errors = data._embedded.errors;
            }
        });
    };
}
