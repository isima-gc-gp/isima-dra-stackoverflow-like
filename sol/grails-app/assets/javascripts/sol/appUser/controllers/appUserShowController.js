//= wrapped

angular
    .module("sol.appUser")
    .controller("AppUserShowController", AppUserShowController);

function AppUserShowController(AppUser, $stateParams, $state) {
    var vm = this;

    AppUser.get({id: $stateParams.id}, function(data) {
        vm.appUser = new AppUser(data);
    }, function() {
        $state.go('appUser.list');
    });

    vm.delete = function() {
        vm.appUser.$delete(function() {
            $state.go('appUser.list');
        }, function() {
            //on error
        });
    };

}
