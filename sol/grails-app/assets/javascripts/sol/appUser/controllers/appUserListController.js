//= wrapped

angular
    .module("sol.appUser")
    .controller("AppUserListController", AppUserListController);

function AppUserListController(AppUser) {
    var vm = this;

    var max = 10, offset = 0;

    AppUser.list({max: max, offset: offset}, function(data) {
        vm.appUserList = data;
    });
}
