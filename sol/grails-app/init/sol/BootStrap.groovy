package sol

import grails.util.Environment
import sol.AppUser
import sol.AppUserRole
import sol.AppUserBadge
import sol.Badge
import sol.Feature
import sol.Question
import sol.Response
import sol.Role
import sol.Tag
import sol.Vote

class BootStrap {

    def init = { servletContext ->
	
		if (Role.count() > 0) // Minimal data set is already present
			return
		
		// Create user roles
		def adminRole = new Role(authority: 'ROLE_ADMIN').save()
		def userRole = new Role(authority: 'ROLE_USER').save()
		assert Role.count() == 2
		
		AppUser adminUser
		AppUser standardUser
	
		// Init default users
		if (Environment.current == Environment.DEVELOPMENT || Environment.current == Environment.TEST)
		{
			def testUser1 = new AppUser(username: 'admin', password: 'admin', email: 'admin@stackoverlike.com').save()
			def testUser2 = new AppUser(username: 'user', password: 'user', email: 'user@example.com').save()
			adminUser = testUser1
			standardUser = testUser2

			AppUserRole.create testUser1, userRole
			AppUserRole.create testUser1, adminRole
			AppUserRole.create testUser2, userRole

			AppUserRole.withSession {
			 it.flush()
			 it.clear()
			}

			assert AppUser.count() == 2
			assert AppUserRole.count() == 3
		}
		else // Environment.current == Environment.PRODUCTION
		{
			def testUser1 = new AppUser(username: 'Administrator', password: 'Dra*2017', email: 'admin@stackoverlike.com').save()
			def testUser2 = new AppUser(username: 'Gandalf', password: 'AWizardIsNeverLate!', email: 'gandalf@middle-earth.tkn').save()
			adminUser = testUser1
			standardUser = testUser2

			AppUserRole.create testUser1, userRole
			AppUserRole.create testUser1, adminRole
			AppUserRole.create testUser2, userRole

			AppUserRole.withSession {
			 it.flush()
			 it.clear()
			}

			assert AppUser.count() == 2
			assert AppUserRole.count() == 3
		}
		
		def tag = new Tag()
		tag.title = 'Misc'
		tag.desc = 'Content about miscellaneous subjects.'
		tag.save()
		
		def tag2 = new Tag()
		tag2.title = 'Grails'
		tag2.desc = 'Grails is an Open Source, full stack, web application framework that uses the Groovy programming language.'
		tag2.save()
		
		def question = new Question()
        question.title = 'What is StackoverLike ?'
		question.content = 'I just discovered this site and it seems quite helpful, but do you not think it could be great to have a quick "tour" page ?'
		question.tags = [tag]
		question.user = standardUser
		question.save()
		
		def response = new Response()
		response.content = 'I am too lazy to make a such page right now.'
		response.question = question
		response.user = adminUser
		response.save()
		
		def response2 = new Response()
		response2.content = 'Well, thinking a bit more about this, I will probably add a such page in the upcoming days.'
		response2.question = question
		response2.user = adminUser
		response2.save()
		
		def vote = new Vote()
		vote.post = question
		vote.isUpVote = true
		vote.originatorUser = adminUser
		vote.save()
		
		def vote2 = new Vote()
		vote2.post = response
		vote2.isUpVote = false
		vote2.originatorUser = standardUser
		vote2.save()
		
		def badge = new Badge()
		badge.code = 'sol.badge.curious'
		badge.save()
		
		def badge2 = new Badge()
		badge2.code = 'sol.badge.educated'
		badge2.save()
		
		def badge3 = new Badge()
		badge3.code = 'sol.badge.critic'
		badge3.save()
		
		def userBadge = new AppUserBadge()
		userBadge.user = standardUser
		userBadge.badge = badge
		userBadge.save()
		
		def userBadge2 = new AppUserBadge()
		userBadge2.user = adminUser
		userBadge2.badge = badge2
		userBadge2.save()
		
		def userBadge3 = new AppUserBadge()
		userBadge3.user = standardUser
		userBadge3.badge = badge3
		userBadge3.save()
		
		def userBadge4 = new AppUserBadge()
		userBadge4.user = adminUser
		userBadge4.badge = badge3
		userBadge4.save()
		
		def featureVote = new Feature()
		featureVote.name = "vote"
		featureVote.enabled = true
		featureVote.save()

		def featureBadg = new Feature()
		featureBadg.name = "badge"
		featureBadg.enabled = true
		featureBadg.save()

		def featureAuth = new Feature()
		featureAuth.name = "auth"
		featureAuth.enabled = true
		featureAuth.save()
		
		def testUser = new AppUser(username: 'Administrator', password: 'Dra*2017', email: 'admin2@stackoverlike.com').save()

		AppUserRole.create testUser, userRole
		AppUserRole.create testUser, adminRole

		AppUserRole.withSession {
		 it.flush()
		 it.clear()
		}
    }
    def destroy = {
    }
}
