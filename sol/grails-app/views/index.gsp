<!doctype html>
<html lang="en" class="no-js">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <title>Stack Overlike</title>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>

    <style type="text/css">
        [ng\:cloak], [ng-cloak], [data-ng-cloak], [x-ng-cloak], .ng-cloak, .x-ng-cloak {
            display: none !important;
        }
    </style>

    <asset:stylesheet src="application.css"/>

    <asset:link rel="icon" href="favicon.ico" type="image/x-ico" />

    <script type="text/javascript">
        window.contextPath = "${request.contextPath}";
    </script>
</head>

<body ng-app="sol" ng-controller="IndexController as indexCtrl">

    <div id="wrapper">
        <div class="navbar navbar-default navbar-static-top" role="navigation">
            <div class="container">
                <div id="navbar-title">
                    <a href="/#">
                        <i class="fa grails-icon">
                            <asset:image src="grails-cupsonly-logo-white.svg"/>
                        </i> Stack Overlike
                    </a>
                </div><!--
                --><div id="navbar-navlinks" class="nav">
                    <a ui-sref="question.list">Questions</a>
                    <a ui-sref="tag.list">Tags</a>
                    <a ui-sref="question.create">Ask</a>
                </div><!--
                --><div id="navbar-weather"></div><!--
                --><div id="navbar-login" class="navbar-collapse collapse" aria-expanded="false" style="height: 0.8px;" uib-collapse="!navExpanded">
                    <ul class="nav navbar-nav" ng-switch="indexCtrl.currentUser == null">
                        <li ng-switch-when="true">
                            <a ui-sref="login">Login</a>
                            <a ui-sref="appUser.create">Sign In</a>
                        </li>
                        <li ng-switch-when="false" class="dropdown" uib-dropdown>
                            <a href="#" class="dropdown-toggle" uib-dropdown-toggle role="button" aria-haspopup="true" aria-expanded="false">{{indexCtrl.currentUser.sub}}<span class="caret"></span></a>
                            <ul class="dropdown-menu" uib-dropdown-menu>
                                <li><a ui-sref="appUser.show({username: indexCtrl.currentUser.sub})">Your profile</a></li>
                                <li><a ui-sref="logout">Logout</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="content" ui-view></div>

        <div class="footer" role="contentinfo">
        </div>

        <div id="spinner" class="spinner" style="display:none;">
            <g:message code="spinner.alt" default="Loading&hellip;"/>
        </div>
    </div>
    <asset:javascript src="/sol/sol.js" />
</body>
</html>
