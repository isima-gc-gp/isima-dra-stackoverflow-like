package sol

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand
import org.springframework.stereotype.Service
import grails.converters.JSON

@Service
public class WeatherService {

  @HystrixCommand(fallbackMethod = "reliable")
  public String weatherInfo() {
    /*def data = new URL("http://api.openweathermap.org/data/2.5/weather?q=Clermont-Ferrand&appid=d47cb22946d4808de6a6f4c0a705f210").getText()
    println data*/
	def data = JSON.parse(new URL("http://api.openweathermap.org/data/2.5/weather?q=Clermont-Ferrand&appid=d47cb22946d4808de6a6f4c0a705f210").getText())
    return "Temp. à Clermont-Ferrand : " + (data.main.temp - 273.15) + "°C";
  }

  public String reliable() {
    return "Température à Cuba : 28°C (comme toujours)";
  }
}