package sol

// Manages the verification of enabled/disabled features (Feature Flipping)
class FeatureFlippingService {

	// Verify if the given feature is enabled
    def withFeature(String featureName) {
    	
    	def feature = Feature.findByName(featureName)

    	if (feature == null)
    	{
    		return false;
    	}

    	return feature.enabled
    }
}
