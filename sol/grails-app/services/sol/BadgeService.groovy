package sol

import grails.transaction.Transactional

// Manages creation of an association between a user and a badge (Service).
@Transactional
class BadgeService {

	// Injection
	def featureFlippingService

	// Add a badge to a user if he doesn't have it yet
    def earnBadgeIfNotAlreadyOwned(AppUser user, String badgeCode) {
    	if (featureFlippingService.withFeature('badge'))
    	{
    		def badge = Badge.findByCode(badgeCode)
			if (badge != null)
			{
				def userBadge = AppUserBadge.findByUserAndBadge(user, badge)
				if (userBadge == null)
				{
					userBadge = new AppUserBadge()
					userBadge.user = user
					userBadge.badge = badge
					userBadge.save flush:true
				}
			}
    	}
    }
}
