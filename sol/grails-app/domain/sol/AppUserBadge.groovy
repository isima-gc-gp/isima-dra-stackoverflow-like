package sol

// Represents an association between a user and a badge (Domain).
class AppUserBadge {
	// Date of obtention
	Date dateCreated

	static belongsTo = [user : AppUser,
						badge : Badge]
	
    static constraints = {
    }
}
