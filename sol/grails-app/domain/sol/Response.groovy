package sol

// Represents a Response (Domain)
class Response extends Post {

	static belongsTo = [question : Question]

    static constraints = {
    }
}
