package sol

import groovy.transform.EqualsAndHashCode
import groovy.transform.ToString

// Represents a user role (user or admin) (Domain).
@EqualsAndHashCode(includes='authority')
@ToString(includes='authority', includeNames=true, includePackage=false)
class Role implements Serializable {

	private static final long serialVersionUID = 1

	// Name of the authority
	String authority

	static constraints = {
		authority blank: false, unique: true
	}

	static mapping = {
		cache true
	}
}
