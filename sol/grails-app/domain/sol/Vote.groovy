package sol

// Represents a vote from a user on a post.
class Vote {

	// True if positive, false otherwise.
	Boolean isUpVote

	static belongsTo = [post : Post,
						originatorUser : AppUser]

    static constraints = {
		originatorUser(validator: { val, obj ->
			if (val != null && obj.post != null && obj.post.user != null && val.id == obj.post.user.id) {
				return false
			}
		})
		post unique: 'originatorUser'
    }
}
