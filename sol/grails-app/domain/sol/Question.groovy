package sol

// Represents a Question (domain).
class Question extends Post {

	// Title of the question
	String	title

	static hasMany = [responses : Response, tags : Tag]

    static constraints = {
		title(blank: false, maxSize: 140)
    }
}
