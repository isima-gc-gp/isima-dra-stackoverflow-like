package sol

// Represents a feature (Domain).
class Feature {
	// Name of the feature
	String name
	Boolean enabled
	
    static constraints = {
		name blank: false, unique: true
    }
}
