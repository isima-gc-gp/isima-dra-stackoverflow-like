package sol

import groovy.transform.EqualsAndHashCode
import groovy.transform.ToString

// Represents a user (Domain).
@EqualsAndHashCode(includes='username')
@ToString(includes='username', includeNames=true, includePackage=false)
class AppUser implements Serializable {

	private static final long serialVersionUID = 1

	transient springSecurityService
	transient featureFlippingService

	String username
	String password
	String email
	boolean enabled = true
	boolean accountExpired
	boolean accountLocked
	boolean passwordExpired

	// Return user's authorities (admin, user, ...)
	Set<Role> getAuthorities() {
		AppUserRole.findAllByAppUser(this)*.role
	}

	// Called before any database insertion
	def beforeInsert() {
		encodePassword()
	}

	// Called before any database information update
	def beforeUpdate() {
		if (isDirty('password')) {
			encodePassword()
		}
	}

	// Encode user password if possible
	protected void encodePassword() {
		password = springSecurityService?.passwordEncoder ? springSecurityService.encodePassword(password) : password
	}

	static transients = ['springSecurityService', 'reputation', 'questions', 'answers', 'badges']

	static constraints = {
		password blank: false, password: true
		username blank: false, unique: true
		email blank: false, unique: true
	}

	static mapping = {
		password column: '`password`'
	}
	
	// Application specific attributes
	
	static hasMany = [originatedVotes : Vote,
					  userBadges : AppUserBadge,
					  posts : Post]

	// Return the reputation of the user
	Long getReputation()
	{
		if (featureFlippingService.withFeature("vote"))
		{
			def userId = this.id
			def upQuery = sol.Vote.where {
	            post.user.id == userId && isUpVote == true
	        }
	        def downQuery = sol.Vote.where {
	            post.user.id == userId && isUpVote == false
	        }

	        // Return positive votes - negative votes
	        return upQuery.count() - downQuery.count()
	    }
	    else
	   	{
	    	return null
	   	}
	}

	// Return the questions asked by the user
    List getQuestions() {
		return Question.findAllByUser(this)
    }

	// Return the answers posted by the user
    List getAnswers() {
		return Response.findAllByUser(this)
    }

    // Return the badges earned by the user
    List getBadges() {
		if (featureFlippingService.withFeature("badge"))
		{
			return this?.userBadges*.badge
		}
		else
		{
			return null
		}
    }
}
