package sol

// Represents a Post (both Questions and Responses) (Domain).
class Post {
    // Content of the post
	String 	content

    // Date of post's creation
	Date	dateCreated

    // Date of last post's update
	Date	lastUpdated

    transient featureFlippingService
	
	static belongsTo = [user : AppUser]

	static	hasMany = [votes : Vote]

    static constraints = {
		content(blank: false, maxSize: 65535)
    }

    static transients = ['voteScore']

    // Return the vote score of the post
    Long getVoteScore()
    {
        if (featureFlippingService.withFeature("vote"))
        {
            def postId = this.id
            def upQuery = sol.Vote.where {
                post.id == postId && isUpVote == true
            }
            def downQuery = sol.Vote.where {
                post.id == postId && isUpVote == false
            }
    
            // Return positive votes - negative votes
            return upQuery.count() - downQuery.count()
        }
        else
        {
            return null
        }
    }
}
