package sol

// Represents a question tag (Domain).
class Tag {

	// Name of the tag
	String title

	// A short description
	String desc

	static hasMany = [questions: Question]
	static belongsTo = Question

    static constraints = {
    	title(blank: false, unique: true, maxSize: 30)
    	desc(blank: false, maxSize: 140)
    }
}
