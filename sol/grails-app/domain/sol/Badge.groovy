package sol

// Represents a badge (Domain).
class Badge {
	// Name of the badge (is an internationalization key)
	String code
	
	static hasMany = [userBadges : AppUserBadge]

    static constraints = {
		code unique: true
    }
}
