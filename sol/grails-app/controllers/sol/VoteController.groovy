package sol

import static org.springframework.http.HttpStatus.*
import grails.plugin.springsecurity.annotation.Secured
import grails.transaction.Transactional

// Manages interactions with vote domain class (Controller).
@Transactional(readOnly = true)
class VoteController {

    static allowedMethods = [save: "POST",
							update: ["PUT", "GET"],
							delete: ["DELETE", "GET"] ]
	
	// Injections
	def springSecurityService
	def badgeService
	def featureFlippingService

	// Create a new vote from a user on a post (user and admin)
	@Secured('ROLE_USER')
    @Transactional
    def addVote() {

    	// If vote's feature is enabled
    	if (featureFlippingService.withFeature('vote'))
    	{
    		params["post"] = Post.findById(params["post"])
			params["originatorUser"] = springSecurityService.currentUser
			
			def vote = new Vote(params)
			if (params["post"].user != params["originatorUser"]) {
				vote.save()
				
				badgeService.earnBadgeIfNotAlreadyOwned(vote.originatorUser, 'sol.badge.critic')
			}

			def questionId = getQuestionIdFromVote(vote)
			redirect controller:"question", action:"show", method:"GET",  params: [id: questionId]
    	}
    	else
    	{
    		notAvailable()
    	}
    }
	
	// Reverse a vote (user and admin)
	@Secured('ROLE_USER')
    @Transactional
	def toggleVote() {

		// If vote's feature is enabled
		if (featureFlippingService.withFeature('vote'))
		{
			def vote = Vote.findById(params["id"])
			
			if (vote.originatorUser == springSecurityService.currentUser) {
				vote.isUpVote = !vote.isUpVote
				
				vote.save()
			}			

			def questionId = getQuestionIdFromVote(vote)
			redirect controller:"question", action:"show", method:"GET",  params: [id: questionId]
		}
    	else
    	{
    		notAvailable()
    	}
	}

	// Delete a vote (user and admin)
	@Secured('ROLE_USER')
    @Transactional
    def delete(Vote vote) {

    	// If vote's feature is enabled
		if (featureFlippingService.withFeature('vote'))
		{
	        if (vote == null) {
	            transactionStatus.setRollbackOnly()
	            notFound()
	            return
	        }

	        vote.delete flush:true

		    def questionId = getQuestionIdFromVote(vote)
			redirect controller:"question", action:"show", method:"GET",  params: [id: questionId]
    	}
    	else
    	{
    		notAvailable()
    	}
    }

    // Vote not found page
    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'vote.label', default: 'Vote'), params.id])
                redirect controller:"question", action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }

	// Vote's feature not available
    protected void notAvailable() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.available.message', args: [message(code: 'vote.label', default: 'Vote')])
                redirect controller:"question", action: "index", method: "GET"
            }
            '*'{ render status: SERVICE_UNAVAILABLE }
        }
    }

	// Return the corresponding question to a vote
	private def getQuestionIdFromVote(Vote vote) {
		def isResponse = vote.post instanceof Response
		def questionId = (!isResponse ? vote.post.id : ((Response)(vote.post)).question.id)
		return questionId
	}
}
