package sol

import grails.converters.JSON
import grails.plugin.springsecurity.annotation.Secured
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;

import groovy.transform.EqualsAndHashCode
import groovy.transform.ToString

// Manages interactions with weather service (Controller).
@EnableCircuitBreaker
class WeatherController {
	
    // Injections
    def weatherService

    // Display weather information (All visitor)
	@Secured('permitAll')
    def index() {
        render "<span>" + weatherService.weatherInfo() + "</span>"
    }
}
