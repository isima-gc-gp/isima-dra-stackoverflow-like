package sol

import static org.springframework.http.HttpStatus.*
import grails.plugin.springsecurity.annotation.Secured
import grails.plugin.springsecurity.SpringSecurityUtils
import grails.transaction.Transactional

// Manages interactions with response domain class (Controller).
@Transactional(readOnly = true)
class ResponseController {

    static allowedMethods = [save: "POST", update: "PUT", delete: ["DELETE", "GET"]]

    // Injections
	def springSecurityService
	def badgeService

    // List all the responses (All visitor) - (to debug purpose only)
    @Secured('permitAll')
    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond Response.list(params), model:[responseCount: Response.count()]
    }

    // Show the detail of a response (All visitor)
    @Secured('permitAll')
    def show(Response response) {

        if (response == null)
        {
            notFound()
            return
        }
        
        respond response
    }

    // Display the creation form (User and Admin)
	@Secured('ROLE_USER')
    def create() {
		def question = Question.findById(params["questionId"])
        respond new Response(params), model: [question: question ]
    }

    // Save a new response if possible (User and Admin)
	@Secured('ROLE_USER')
    @Transactional
    def save(Response response) {
		
        if (response == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }
		
		if (response.user == null)
			response.user = springSecurityService.currentUser
		response.validate()

        if (response.hasErrors()) {
            transactionStatus.setRollbackOnly()
            respond response.errors, view:'create'
            return
        }
		
        response.save flush:true
		
		badgeService.earnBadgeIfNotAlreadyOwned(response.user, 'sol.badge.educated')

		respond status: CREATED
    }

    // Display the edition form (User and Admin)
	@Secured('ROLE_USER')
    def edit(Response response) {
        if (response == null) {
            notFound()
            return
        }
		
        respond response
    }

    // Update an already existing response (User and Admin)
	@Secured('ROLE_USER')
    @Transactional
    def update(Response response) {
        if (response == null || !isAdminOrAuthor(response)) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        if (response.hasErrors()) {
            transactionStatus.setRollbackOnly()
            respond response.errors, view:'edit'
            return
        }

        response.save flush:true

		respond status: OK
    }

    // Deletion of a response (already confirmed)
	@Secured('ROLE_USER')
    @Transactional
    def delete(Response response) {

        if (response == null || !isAdminOrAuthor(response)) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        response.delete flush:true

        respond status: OK
    }

    // Response not found page
    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'response.label', default: 'Response'), params.id])
                redirect controller: "question", action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
	
	private boolean isAdminOrAuthor(Response response) {
		return springSecurityService != null && (SpringSecurityUtils.ifAllGranted("ROLE_ADMIN") || response.user == springSecurityService?.currentUser)
	}
}
