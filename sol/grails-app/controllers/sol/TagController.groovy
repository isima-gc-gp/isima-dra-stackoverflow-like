package sol

import static org.springframework.http.HttpStatus.*
import grails.plugin.springsecurity.annotation.Secured
import grails.transaction.Transactional

// Manages interactions with tag domain class (Controller).
@Transactional(readOnly = true)
class TagController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    // List all the tags (All visitor)
	@Secured('permitAll')
    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond Tag.list(params), model:[tagCount: Tag.count()]
    }

    // Display the detail of a tag (All visitor)
	@Secured('permitAll')
    def show(Tag tag) {
        respond tag
    }

    // Display the creation form (only available for administrators)
	@Secured('ROLE_ADMIN')
    def create() {
        respond new Tag(params)
    }

    // Save a new tag if possible(only available for administrators)
	@Secured('ROLE_ADMIN')
    @Transactional
    def save(Tag tag) {
        if (tag == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        if (tag.hasErrors()) {
            transactionStatus.setRollbackOnly()
            respond tag.errors, view:'create'
            return
        }

        tag.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'tag.label', default: 'Tag'), tag.id])
                redirect tag
            }
            '*' { respond tag, [status: CREATED] }
        }
    }

    // Display the edition form (only available for administrators)
	@Secured('ROLE_ADMIN')
    def edit(Tag tag) {
        respond tag
    }

    // Update an already existing tag (only available for administrators)
	@Secured('ROLE_ADMIN')
    @Transactional
    def update(Tag tag) {
        if (tag == null) {
            notFound()
            return
        }

        if (tag.hasErrors()) {
            respond tag.errors, view:'edit'
            return
        }

        tag.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'tag.label', default: 'Tag'), tag.id])
                redirect tag
            }
            '*'{ respond tag, [status: OK] }
        }
    }

    // Deletion of a tag (already confirmed) (only available for administrators)
	@Secured('ROLE_ADMIN')
    @Transactional
    def delete(Tag tag) {

        if (tag == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

		tag.questions.each {
			tag.removeFromQuestions(it)
		}
        tag.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'tag.label', default: 'Tag'), tag.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    // Tag not found page
	@Secured('permitAll')
    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'tag.label', default: 'Tag'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
