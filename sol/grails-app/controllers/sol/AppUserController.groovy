package sol

import static org.springframework.http.HttpStatus.*
import grails.plugin.springsecurity.annotation.Secured
import grails.plugin.springsecurity.SpringSecurityUtils
import grails.transaction.Transactional
import grails.converters.JSON

import groovy.transform.EqualsAndHashCode
import groovy.transform.ToString

// Manages interactions with user domain class (Controller).
@Transactional(readOnly = true)
class AppUserController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]
	
    // Injections
	def springSecurityService
    def passwordEncoder
    def featureFlippingService

    // List all the users (only available for adminisators)
	@Secured('ROLE_ADMIN')
    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond AppUser.list(params), model:[appUserCount: AppUser.count()]
    }

    // Display the detail of a user (All visitor)
	@Secured('permitAll')
    def show(AppUser appUser) {
        println "Here"

        // If the corresponding user is not found, redirection
        if (appUser == null) {
            notFound()
            return
        }
		
        // Retrieve questions, answers and badges (ordered by dates)
		def orderedQuestions = appUser.questions.sort { a, b ->
			return -(a.lastUpdated <=> b.lastUpdated) /* desc */
		}

		def orderedAnswers = appUser.answers.sort { a, b ->
			return -(a.lastUpdated <=> b.lastUpdated) /* desc */
		}

		def orderedBadges = null

        if (featureFlippingService.withFeature('badge'))
        {
            orderedBadges = sol.AppUserBadge.findAllByUser(appUser).sort {a, b ->
                return -(a.dateCreated <=> b.dateCreated) /* desc */   
            }*.badge
        }

		respond appUser, model: [badges: orderedBadges, 
								questions: orderedQuestions, 
								answers: orderedAnswers]
    }

    // Display the creation form (All visitor)
	@Secured('permitAll')
    def create() {
        if (featureFlippingService.withFeature('auth'))
        {
            respond new AppUser(params)
        }
        else
        {
            notAvailable()
        }
    }

    // Save a new user if possible (All visitor)
	@Secured('permitAll')
    @Transactional
    def save(AppUser appUser) {
        if (featureFlippingService.withFeature('auth'))
        {
            // If the corresponding user is not found, redirection
            if (appUser == null) {
                transactionStatus.setRollbackOnly()
                notFound()
                return
            }

            // If some user's properties violate their constraints
            if (appUser.hasErrors()) {
                transactionStatus.setRollbackOnly()
                respond appUser.errors, view:'create'
                return
            }

            // If the user is OK
            appUser.save flush:true

            // Set user's role to user
            def userRole = sol.Role.findByAuthority('ROLE_USER')
            AppUserRole.create appUser, userRole

            // Redirect to profile page
            request.withFormat {
                form multipartForm {
                    flash.message = message(code: 'default.created.message', args: [message(code: 'appUser.label', default: 'AppUser'), appUser.id])
                    redirect appUser
                }
                '*' { respond appUser, [status: CREATED] }
            }
        }
        else
        {
            notAvailable()
        }
    }

    // Display the edition form (User and Admin)
	@Secured('ROLE_USER')
    def edit(AppUser appUser) { 
        if (featureFlippingService.withFeature('auth'))
        {
            respond appUser
        }
        else
        {
            notAvailable()
        }
    }

    // Update an already existing user (User and Admin)
	@Secured('ROLE_USER')
    @Transactional
    def update(AppUser appUser) {
		
        if (featureFlippingService.withFeature('auth'))
        {
            // If the corresponding user is not found, redirection
            if (appUser == null || appUser != springSecurityService?.currentUser) {
                transactionStatus.setRollbackOnly()
                notFound()
                return
            }

            appUser.validate()

            // If some user's properties violate their constraints OR if the user didn't confirm password well
            if (appUser.hasErrors() || flash.errors != null) {
                transactionStatus.setRollbackOnly()
                respond appUser.errors, view:'edit'
                return
            }

            appUser.save flush:true
			
            respond status: OK
        }
        else
        {
            notAvailable()
        }
    }

    // Deletion of a user (already confirmed)
    @Transactional
    def delete(AppUser appUser) {

        if (featureFlippingService.withFeature('auth'))
        {
            // If the corresponding user is not found, redirection
            if (appUser == null) {
                transactionStatus.setRollbackOnly()
                notFound()
                return
            }

            appUser.delete flush:true

            request.withFormat {
                form multipartForm {
                    flash.message = message(code: 'default.deleted.message', args: [message(code: 'appUser.label', default: 'AppUser'), appUser.id])
                    redirect action:"index", method:"GET"
                }
                '*'{ render status: NO_CONTENT }
            }
        }
        else
        {
            notAvailable()
        }
    }

    // User not found page
    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'appUser.label', default: 'AppUser'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }

    // User feature not available
    protected void notAvailable() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.available.message', args: [message(code: 'appUser.label', default: 'AppUser'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: SERVICE_UNAVAILABLE }
        }
    }
}
