package sol

import static org.springframework.http.HttpStatus.*
import grails.plugin.springsecurity.annotation.Secured
import grails.plugin.springsecurity.SpringSecurityUtils
import grails.transaction.Transactional

// Manages interactions with Question domain class (Controller).
@Transactional(readOnly = true)
class QuestionController {

    static allowedMethods = [save: "POST", update: "PUT", delete: ["DELETE", "GET"]]

    // Injections
	def springSecurityService
	def badgeService
    def featureFlippingService
	
    // List all the questions (All visitors)
	@Secured('permitAll')
    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond Question.list(params), model:[questionCount: Question.count()]
    }

    // Show the detail of a question (All visitor)
	@Secured('permitAll')
    def show(Question question) {

        if (question == null)
        {
            notFound()
            return
        }
		
        def orderedResponses
        // Feature flipping on vote
        if(featureFlippingService.withFeature('vote'))
        {
            // Sort by vote and then by date
            orderedResponses = question?.responses?.sort { a, b ->
                def scorePlusA = Vote.countByPostAndIsUpVote(a, true)
                def scoreMinusA = Vote.countByPostAndIsUpVote(a, false)
                def scorePlusB = Vote.countByPostAndIsUpVote(b, true)
                def scoreMinusB = Vote.countByPostAndIsUpVote(b, false)
                def scoreA = scorePlusA - scoreMinusA
                def scoreB = scorePlusB - scoreMinusB
                if (scoreA == scoreB)
                    return a.dateCreated <=> b.dateCreated /*asc*/
                return -scoreA <=> -scoreB /*desc*/
            }
        }
        else
        {
            // Sort by date
            orderedResponses = question?.responses?.sort { a, b ->
                return a.dateCreated <=> b.dateCreated /*asc*/
            }
        }

		respond question, model: [orderedResponses: orderedResponses]
    }

    // Display the creation form (User and Admin)
    @Secured('ROLE_USER')
    def create() {
        respond new Question(params)
    }

    // Save a new question if possible (User and Admin)
	@Secured('ROLE_USER')
    @Transactional
    def save(Question question) {

        if (question == null) {
            transactionStatus.setRollbackOnly()
            render status: UNAUTHORIZED
            return
        }
        
		if (question.user == null && springSecurityService.currentUser != null)
        {
			question.user = springSecurityService.currentUser
            question.validate()
        }
        else
        {
            transactionStatus.setRollbackOnly()
            render status: UNAUTHORIZED
            return
        }

        if (question.hasErrors()) {
            transactionStatus.setRollbackOnly()
            respond question.errors, view:'create'
            return
        }

        question.save flush:true
		
		badgeService.earnBadgeIfNotAlreadyOwned(question.user, 'sol.badge.curious')

        respond status: CREATED
    }

    // Display the edition form (User and Admin)
	@Secured('ROLE_USER')
    def edit(Question question) {
        respond question
    }

    // Update an already existing question (User and Admin)
	@Secured('ROLE_USER')
    @Transactional
    def update(Question question) {

        if (question == null || !isAdminOrAuthor(question)) {
            transactionStatus.setRollbackOnly()
            render status: UNAUTHORIZED
            return
        }

        if (question.hasErrors()) {
            transactionStatus.setRollbackOnly()
            respond question.errors, view:'edit'
            return
        }

        question.save flush:true

        respond status: OK
    }

    // Deletion of a question (already confirmed)
	@Secured('ROLE_USER')
    @Transactional
    def delete(Question question) {

        if (question == null || !isAdminOrAuthor(question)) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        question.delete flush:true

		flash.message = message(code: 'default.deleted.message', args: [message(code: 'question.label', default: 'Question'), question.id])
        
        respond status: OK
    }

    // Question not found page
    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'question.label', default: 'Question'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
	
	private boolean isAdminOrAuthor(Question question) {
		return springSecurityService != null && (SpringSecurityUtils.ifAllGranted("ROLE_ADMIN") || question.user == springSecurityService?.currentUser)
	}
}
