package sol

import static org.springframework.http.HttpStatus.*
import grails.plugin.springsecurity.annotation.Secured
import grails.plugin.springsecurity.SpringSecurityUtils
import grails.transaction.Transactional

// Manages interactions with Feature domain class (Controller).
@Transactional(readOnly = true)
class FeatureController {

	// Allow to enable/disable a feature
    def index() 
    {
    	def feature = Feature.findByName(params["name"])
    	if (feature == null)
        {
    		notFound()
        }

       	feature.enabled = Boolean.parseBoolean(params["enabled"])
    	feature.save flush:true

    	flash.message = message(code: "default.feature.flipping.${feature.enabled}", args: [feature.name], default: "Feature {0} has changed")

    	redirect(uri:'/')
    }

    def notFound()
    {
    	request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'feature.label', default: 'Feature'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
